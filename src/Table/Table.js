import React from 'react';
import './Table.css';

export default props => (
    <table className="table mytable">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Username</th>
                <th>E-mail</th>
                <th>Street</th>
                <th>Suite</th>
                <th>City</th>
                <th>Zipcode</th>
                <th>Geo</th>
                <th>Phone</th>
                <th>Website</th>
                <th>Company name</th>
                <th>Company catch phrase</th>
                <th>Company BS</th>
            </tr>
        </thead>
        <tbody>
            { props.data.map(item =>(
                <tr key={item.id + item.email}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.username}</td>
                    <td>{item.email}</td>
                    <td>{item.address.street}</td>
                    <td>{item.address.suite}</td>
                    <td>{item.address.city}</td>
                    <td>{item.address.zipcode}</td>
                    <td>{item.address.geo.lat + '   ' + item.address.geo.lng}</td>
                    <td>{item.phone}</td>
                    <td>{item.website}</td>
                    <td>{item.company.name}</td>
                    <td>{item.company.catchPhrase}</td>
                    <td>{item.company.bs}</td>
                </tr>
            ))}
        </tbody>
    </table>
)

